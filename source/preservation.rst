.. include:: images.rst

.. _preservation-label:

Preservation
===============================================================================

.. Admonition:: Version

   This module was introduced in version 23.11 of Koha.

The preservation module can be accessed in two different ways: on the main page of the staff interface by
clicking 'Preservation' or through the 'More' menu on the top of the page.

The preservation module in Koha is used for integrating preservation treatments into the workflow and
keep track of them. For every single step of the preservation workflow, data is attached to the items. 
For each workflow, items go to a waiting list before being added to a train.

In order to use the module, you will first need to enable it using the  
:ref:`PreservationModule system preference<preservationmodule-syspref-label>`. Second,
:ref:`add the statuses<add-new-authorized-value-label>` you will need to track your items
throughout your workflow to the NOT_LOAN authorized value category.

- *Get there:* More > Preservation

.. _preservation-settings-label:

Settings
-------------------------------------------------------------------------------

The preservation settings are meant for two types of information:

#. storing the chosen statuses (set up as NOT_LOAN :ref:`authorized values <authorized-values-label>`) for the workflow;

#. adding new processings that are meant to be attached to trains.
 
-  *Get there:* More > Preservation > Settings

|preservationsettings|

.. _preservation-workflow-statuses:

Preservation workflow item statuses 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this section, you can set two different statuses chosen from the NOT_LOAN
:ref:`authorized values <authorized-values-label>` category: 

-  Status for item added to waiting list: default items.notforloan status for each item entered in the
   waiting list (beginning of the preservation workflow).

-  Default status for item added to train: default items.notforloan status for any items entered in a
   train (monitoring of preservation workflow).

It is also possible to set these through the :ref:`PreservationNotForLoanWaitingListIn <preservationnotforloanwaitinglistin-label>`
and :ref:`PreservationNotForLoanDefaultTrainIn <preservationnotforloandefaulttrainin-label>` system preferences.

.. _preservation-processing_label:

Add a new processing
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To add a new processing, click on the 'Add new processing' button.

|newprocessing| 

- Processing name: this is the name that will appear in the drop-down menu when creating a new train. 

- Letter template for printing slip: allows to print slips inside the trains if this processing is chosen.

Each processing owns a list of attributes. To add a new attribute, click on the 'Add new attribute' button. 

- Name: the name of the attribute. It will appear as one of the heads of the train table the processing has been attached to. 

- Type: The type of data that will be needed for the train. It can be an authorized value, free text or a database column.

- Options: appears if a type of authorized value or database column has been selected.
   It will show a drop-down menu of the possible values that are to be extracted for the said attribute. 

.. Tip ::

   For 'Type' and 'Options' fields, autocompletion is available: start typing in the box to quickly find
   the desired value.

.. Note ::

   Be sure to have your processings well figured out before creating them on Koha. While processings may be
   editable afterwards, we recommend not to do so in case they have already been attached to a train.
   
   If you do need to make changes, it is advised to create a new processing instead. 
    
.. _preservation-waitinglist-label:

Waiting list	
-------------------------------------------------------------------------------

The waiting list is the first step of the preservation workflow. It is to be considered as the Preservation module entrance hall for the items.
 
-  *Get there:* More > Preservation > Waiting list

When added to the waiting list, the item will have its items.notforloan modified. As long as its status don't change, it will remain in the waiting list. 
It permits to render the item unavailable to the public even though its preservation processes to apply are not sorted yed. 

|waitinglist| 


Add to waiting list
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To add items to the waiting list, click on the 'Add to waiting list' button.

|addtowaitinglist| 

A pop-up window appears, with a list box waiting for items.barcodes to be filled with.
    .. Note::
       If entering a list of barcodes, be sure to have one barcode per line, without space or comas at the end of the barcode. 
       It would not be able to add the whole list, only the last barcode it encounters. 
 
Add last x items to a train
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Right after adding a list of items in the waiting list, to directly add those items to a train, click on the 'Add last x items to a train' button. 
x refers to the number of items you just added. 
It helps fastforwarding a list of items into a train. 

|addlastitemstotrain| 

You will find a drop-down menu containing the list of currently available preservation trains. Select the train, then click 'Submit'. 

.. _preservation-trainspage-label:

Trains
-------------------------------------------------------------------------------

The trains page is the hub of the preservation module. This is where you will create and monitor all your preservation trains. 

    .. Note:: 
       The term "train" has been chosen for two main reasons. Firstly because all of the other terminology 'cart', 'basket', batch, etc. were already used for other modules in Koha.
       Secondly, this is a commonly used terminology by French librarians, since the physical carts aligned look like linked wagons.   
   
-  *Get there:* More > Preservation > Trains

|trainspage| 

The table contains the list of all your trains. The table contains the following information : 

- Name : the name you gave to the train

- Created on : the date the train was initiated 

- Closed on : the date you decided no item can be added to the train anymore

- Sent on : the date all the train is physically sent to the bookbinder

- Received on : the date the train is physically returned to the library

- Actions : 

  - Edit : button that sends back to the settings of the train 

  - Delete : to remove the train from the page

  - Add items : to directly add items from the waiting list to the train with a barcode
    

.. _preservation-train-label:
 
New train
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To create a train, click on the 'New train' button.

|newtrain|

The new train page is a form that you will need to fill for it to appear on the trains pages.
Here are the fields to fill :

- Name: The name you want to give to the train

- Description: a free form to write anything you would need to tell about this train

- Status for item added to this train: Drop-down menu with the NOT_LOAN authorized values. The default value proposed is the one you did set in
  :ref:`system preferences<preservation-system-preferences-label>`
  or in the Preservation module settings page.

- default processing: the processing you want your train attached to, in a drop-down menu.

Click on the 'Submit' button to validate and return to the trains page.

Show train
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To get into a preservation train, click on the train name in the trains page table.

-  *Get there:* More > Preservation > Trains > Show train

The train allows you four different actions:

- Add items: click on the button and a pop-up to add an item into the preservation train will appear.

- Edit: the edit button will send you back to the train settings you did complete in order to create the train.

- Delete: deletes the entire train, and also what's in it.

- Close/Sent/Received: a button to timestamp and monitor the workflow of your preservation train.

Under those four buttons appears the information about the current train settings with, below the items tables included in the train.

    .. Note::
       The head columns in the table match exactly the attributes of the processing you did set beforehand.
       If the default NOT_LOAN for the waiting list and the train are different, the item disappears from the waiting when added to the train. 
       It appears in both otherwise.

    .. Warning::
       We recommend to keep the default processing the same the whole workflow of the preservation train.
       Changing it while the train is full of items will disable the table view of items. 

|mypreservationtrain| 

In the items table inside the preservation train, you will find all the information you did set when adding an item. 
On the right of the same table,  you have three actions : 

- Edit : goes to the item edition page to change the values of its attributes.

- Remove : to remove the item from the train.  

- Print slip : When enabled in the processings' default settings, the 'Print slip' button appears in the actions. 
  It edits a slip with all the information containes in the table for the said item. The style is editable with CSS. 

    .. Warning:: 
       When removing an item or deleting an entire train, the items.notforloan does not go back automatically to the Available status. 
    
    .. Note:: 
       You can also print slips in batch by checking the box on the left of the wanted items, then click on 'Print slips' at the top of the items table.

Following the train workflow, once all the items were added and edited. You will need to close the train. Click on the 'Close' button on the top of the page.
The information of the train will add a 'Closed on' information in date format. From this moment, it becomes impossible to add next items to the train.

The next step is sending physically the train to the bookbinder, and to keep track of this information in Koha, at the exact same space the 'Close' button was, you will find a 'Send' button. 
It will change the status of the train to Sent and add an 'Sent on' information in date format.

Finally, when the train is physically returned to the library, you can click on the 'Receive' button which adds a 'Received on' information in date format. 

|receivedtrain|

From this moment on, let us say the results were not as good as expected, you can move any item in another created preservation train. 
It will keep the preservation data from the previous train, which will be also editable. 


    .. Note:: 
       If the preservation train possesses items with diverse processings, the table won't show as it different tables with different columns cannot coexist together in a unique view.
       In this case, instead of the table will appear a raw item list with all its concording processing data. 
       |eclectictrain|

Add items
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To add an item to the train, click on the 'Add items' button on the top of the page, inside a preservation train.

|addpreservationitem| 

You arrive on a form, where all the attributes of the processing you chose beforehand are written. 
All the fields that expect data from the koha database are extracted and the authorized values' drop-down menu are available.   
All of the fields possess two features : 

   - They are all editable, even when extracted from the koha database or the authorized values. 
   
   - All the fields can be duplicated to have multivaluated fields.
    
|addpreservationitemmultivalued| 

You can also change the needed fields for this specific item, by choosing the concording processing for this item. 

    .. Warning::
       Beware of changing the processings of an item, the train won't be able to show a correct table. 

    .. Note:: 
       None of the modifications here will update the Marc or the authorized values. The modified data are solely meant for the preservation module.



