.. include:: images.rst

.. _preservation-system-preferences-label:

Preservation
-------------------------------------------------------------------------------

.. Admonition:: Version

   This set of system preferences was introduced in version 23.11 of Koha.

The preservation management tab in Koha will give you settings to enable and configure the module for the first time. 

-  *Get there:* More > Administration > System Preferences > Preservation

.. _preservation-interface-label: 

Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _preservationmodule-syspref-label:

PreservationModule
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Asks: \_\_\_ the preservation module

Values:

-  Disable

-  Enable

Default: Disable

Description:

-  This preference controls whether or not staff can use the preservation module.

-  This is the main switch for the preservation module.

.. _preservationnotforloandefaulttrainin-label:

PreservationNotForLoanDefaultTrainIn
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Asks: Use the NOT_LOAN authorized value \_\_\_ to apply to items added to a train

Description:

-  This preference allows libraries to choose the default 'Not for loan' status applied to an item
   when it moves from the preservation waiting list to a preservation train.

-  The authorized value (not its description) from the NOT_LOAN :ref:`authorized values <authorized-values-label>`
   category, should be entered here; e.g., :code:`5`.

-  If left empty, the item's 'Not for loan' status will not be updated when it moves to a preservation train.

-  This system preference corresponds to the :ref:`Preservation module setting<preservation-workflow-statuses>`
   'Default status for item added to train'. Updating one will update the other. 


.. _preservationnotforloanwaitinglistin-label:

PreservationNotForLoanWaitingListIn
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Asks: Use the NOT_LOAN authorized value \_\_\_ to apply to items added to the waiting list

Description: 

-  This preference allows libraries to choose the default  'Not for loan' status applied to an item
   when it is added in the preservation module.

-  The authorized value (not its description) from the NOT_LOAN :ref:`authorized values <authorized-values-label>`
   category, should be entered here; e.g., :code:`-2`.

-  If left empty, the item's 'Not for loan' status will not be updated when it is added to the preservation waiting list.

-  This system preference corresponds to the :ref:`Preservation module setting<preservation-workflow-statuses>`
   'Status for item added to waiting list'. Updating one will update the other. 
